import { EditorState, Editor, Modifier } from 'draft-js';
import React from 'react';
import axios from 'axios';

import './CallNotes.css';
import 'draft-js/dist/Draft.css';
import { SelectionState } from 'draft-js';

const styleMap = {
    SPELLING: {
        textDecoration: 'underline wavy red'
    },
    GRAMMAR: {
        textDecoration: 'underline wavy green'
    }
}

function CallNotes() {

    const [editorState, setEditorState] = React.useState(() => EditorState.createEmpty());
    const [spellingErrors, setSpellingErrors] = React.useState([]); // typeof { type: string, start: number, end: number, suggestions: string[], word: string}
    const [currentError, setCurrentError] = React.useState(null);
    const editor = React.useRef(null);

    const convertIndexToBlockKeyAndOffset = (index) => {
        const blocks = editorState.getCurrentContent().getBlockMap();
        let currentIndex = index;
        const block = blocks.find((b, i) => {
            if (b.getLength() >= currentIndex) {
                return true;
            } else {
                currentIndex -= b.getLength() + 1; // +1 is to represent new lines between each block as a character
                return false;
            }
        })

        return { blockKey: block.getKey(), index: currentIndex }
    }

    const convertBlockKeyAndOffsetToIndex = (blockKey, offset) => {
        const blocks = editorState.getCurrentContent().getBlockMap();
        let index = 0;
        blocks.find(b => {
            if (b.getKey() === blockKey) return true;
            index += b.getLength() + 1; // +1 mimics new lines as a character which gets removed by draftJS
            return false;
        })
        return index + offset;
    }

    const highlightText = (spellCheckErrors = []) => {
        const contentState = editorState.getCurrentContent();

        const unstyledContentState = removeAllStylingFromContentState(contentState);

        const selection = SelectionState.createEmpty();


        const contentWithHighlightedErrors = spellCheckErrors.reduce((newContentState, { type, start, end }) => {
            const startBlockInfo = convertIndexToBlockKeyAndOffset(start);
            const endBlockInfo = convertIndexToBlockKeyAndOffset(end);
            const selectionOption = {
                anchorKey: startBlockInfo.blockKey,
                anchorOffset: startBlockInfo.index,
                focusKey: endBlockInfo.blockKey,
                focusOffset: endBlockInfo.index
            }
            const selectionStateWithNewFocusOffset = selection.merge(selectionOption);
            return Modifier.applyInlineStyle(
                newContentState, selectionStateWithNewFocusOffset, type)
        }, unstyledContentState)


        const newEditorState = EditorState.push(editorState, contentWithHighlightedErrors, 'change-inline-style')
        setEditorState(newEditorState);
    }

    const removeAllStylingFromContentState = (contentState) => {
        const firstBlockKey = contentState.getFirstBlock().getKey();
        const lastBlock = contentState.getLastBlock();
        const selectionState = SelectionState.createEmpty();

        const fullSelection = selectionState.merge({ anchorKey: firstBlockKey, anchorOffset: 0, focusKey: lastBlock.getKey(), focusOffset: lastBlock.getLength() });

        let noStyledContent = Modifier.removeInlineStyle(contentState, fullSelection, 'SPELLING')
        noStyledContent = Modifier.removeInlineStyle(noStyledContent, fullSelection, 'GRAMMAR')

        return noStyledContent;
    }

    const removeAllStyling = () => {
        const contentState = editorState.getCurrentContent();
        const unstyledContentState = removeAllStylingFromContentState(contentState);

        const newEditorState = EditorState.push(editorState, unstyledContentState, 'change-inline-style')
        setEditorState(newEditorState);
    }

    const spellCheck = async () => {
        setCurrentError(null)
        const currentText = editorState.getCurrentContent().getPlainText();
        let startPos = 0;
        const spellCheckResults = await callAPI(currentText);
        const mappedSpellCheckErrors = spellCheckResults.reduce((arr, error) => {
            const idx = currentText.indexOf(error.word, startPos);
            startPos = idx + error.word.length;
            return [...arr, { type: error.type === 'SPELLING' ? 'SPELLING' : 'GRAMMAR', start: idx, end: idx + error.word.length, suggestions: error.suggestedImprovements, word: error.word }]
        }, [])

        setSpellingErrors(mappedSpellCheckErrors) // this is useful for when getting suggested words
        highlightText(mappedSpellCheckErrors);
    }

    const callAPI = async (text) => {
        const params = new URLSearchParams({ 'text-input': text })
        const result = await axios.get('http://localhost:8888/spelling-errors', { params })
        return result.data;
    }

    const fixError = (e) => {
        e.preventDefault();
        const { start, end } = getCursorLocation();
        if (start.blockKey === end.blockKey && start.index === end.index) { // this implies it was a cursor click and not held to select multiple characters
            const indexOfAllText = convertBlockKeyAndOffsetToIndex(start.blockKey, start.index);
            const selectedError = getSelectedError(indexOfAllText)
            setCurrentError(selectedError)
        } else {
            setCurrentError(null)
        }
    }

    const getSelectedError = (index) => {
        return spellingErrors.find(errors => index >= errors.start && index <= errors.end)
    }


    const getCursorLocation = () => {
        const currentSelection = editorState.getSelection();
        const start = { blockKey: currentSelection.getStartKey(), index: currentSelection.getAnchorOffset() }
        const end = { blockKey: currentSelection.getEndKey(), index: currentSelection.getFocusOffset() }
        return { start, end }
    }

    const replaceText = (start, end, replaceWith) => {
        const contentState = editorState.getCurrentContent();

        const startBlockInfo = convertIndexToBlockKeyAndOffset(start);
        const endBlockInfo = convertIndexToBlockKeyAndOffset(end);

        let selection = SelectionState.createEmpty();
        selection = selection.merge({
            anchorKey: startBlockInfo.blockKey,
            anchorOffset: startBlockInfo.index,
            focusKey: endBlockInfo.blockKey,
            focusOffset: endBlockInfo.index
        })

        const newContentState = Modifier.replaceText(contentState, selection, replaceWith);
        
        const newEditorState = EditorState.push(editorState, newContentState, 'insert-characters')
        setEditorState(newEditorState);

        setCurrentError(null)
        
        // remove fixed error:
        const index = spellingErrors.findIndex(error => error.word === currentError.word);

        const newSpellingErrors = [...spellingErrors];
        newSpellingErrors.splice(index, 1);
        setSpellingErrors(newSpellingErrors);
    }

    return (
        <div className="page">

            <h4>Call Notes:</h4>
            <div className="textbox" style={{ border: "1px solid black", minHeight: "6em", cursor: "text" }} onClick={fixError}>
                <Editor
                    ref={editor}
                    customStyleMap={styleMap}
                    editorState={editorState}
                    onChange={setEditorState}
                    placeholder="Write something!"
                />
            </div>
            <div>Number of errors: {spellingErrors.length}</div>

            <button style={{ marginTop: '1em' }} onClick={spellCheck}>Spellcheck</button>
            <br />
            <br />
            <br />

            {!!currentError && <div style= {{padding: '0 1em 1em 1em', border: '1px solid black'}}>
                <h4 style={{textDecoration: 'underline'}}>{currentError.type} error detected: "{currentError.word}"</h4>
                <h5>Suggestions:</h5>
                {currentError.suggestions.slice(0, 5).map((s, i) => <button style={{marginRight: '1em'}} onClick={() => replaceText(currentError.start, currentError.end, s)} key={i}>{s}</button>)}
                </div>
            }
        </div>
    );
}

export default CallNotes