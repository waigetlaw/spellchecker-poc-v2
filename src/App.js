import logo from './logo.svg';
import './App.css';
import CallNotes from './CallNotes';

function App() {
  return (
    <div className="App">
      <CallNotes/>
    </div>
  );
}

export default App;
